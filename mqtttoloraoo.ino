/*
 * uMQTTBroker demo for Arduino
 * 
 * Minimal Demo: the program simply starts a broker and waits for any client to connect.
 */
#include <SPI.h>
#include <LoRa.h>
#include <Wire.h>  
#include <ESP8266WiFi.h>
#include "uMQTTBroker.h"
// Pin definetion LoRa
#include "wemos_definitions.h"
//Lora struct
//typedef struct {
//  byte src_addr;
//  char msg[255];
//  unsigned long count;
// } _l_packet;
//
//_l_packet pkt;
//boolean gotpacket;
void sendMessage(String outgoing){
        // send packet
      LoRa.beginPacket();
      LoRa.print(outgoing);
//      LoRa.print(counter);
      LoRa.endPacket(true);
      Serial.println(outgoing);
      startWiFiAP();
//    counter++;
}
//uMQTTBroker struct
class myMQTTBroker: public uMQTTBroker
{
public:
    virtual bool onConnect(IPAddress addr, uint16_t client_count) {
      Serial.println(addr.toString()+" connected");
      return true;
    }
    
    virtual bool onAuth(String username, String password) {
      Serial.println("Username/Password: "+username+"/"+password);
      return true;
    }
    
    virtual void onData(String topic, const char *data, uint32_t length) {
      char data_str[length+1];
      os_memcpy(data_str, data, length);
      data_str[length] = '\0';
      WiFi.softAPdisconnect(true);
      WiFi.softAPdisconnect(true);
      MQTT_server_cleanupClientCons();
      sendMessage((String)data_str);
//      Serial.println("received topic '"+topic+"' with data '"+(String)data_str+"'");
    }
};

int counter = 0;
myMQTTBroker myBroker;


/*
 * Your WiFi config here
 */
char ssid[] = "uMQTT2LoRa";      // your network SSID (name)
char pass[] = "ChangeMe"; // your network password

/*
 * WiFi init stuff
 */
void startWiFiClient()
{
  Serial.println("Connecting to "+(String)ssid);
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  
  Serial.println("WiFi connected");
  Serial.println("IP address: " + WiFi.localIP().toString());
}

void startWiFiAP()
{
  WiFi.softAP(ssid, pass);
  Serial.println("AP started");
  Serial.println("IP address: " + WiFi.softAPIP().toString());
}
void initlora(){
//  SPI.begin(SCK,MISO,MOSI,SS);
  LoRa.setPins(SS,RST,DI0);
  
//  while (!LoRa.begin(BAND,PABOOST))
Serial.println("Initialising LoRa module....");
  while (!LoRa.begin(BAND))
  {
    Serial.print(".");
  }
  
  Serial.println("LoRa Init success!");
  delay(1000);
  
//  LoRa.onReceive(onReceive);
  
 }


//void onReceive(int packetSize)
//{
//  
// // byte *buff;
// // buff = (byte *)malloc(packetSize);
// // LoRa.readBytes(buff,packetSize);
//  LoRa.readBytes((uint8_t *)&pkt,packetSize); 
//  gotpacket = true;
//  //memcpy((void *)&pkt.src_addr,(void *)buff,1);
//  //memcpy((void *)&pkt.msg,(void *)(buff+1),packetSize-1);
//  pkt.msg[packetSize-1]='\0';
//  pkt.count++;
//  //free(buff);
//  
//}

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();
//  gotpacket = false;
//  pkt.src_addr = 0;
  //pkt.msg;
//  pkt.count = 0;
  // Connect to a WiFi network
//  startWiFiClient();
  // Or start the ESP as AP
  startWiFiAP();
  // Start the broker
  Serial.println("Starting MQTT broker");
  myBroker.init();
  /*
 * Subscribe to anything
 */
  myBroker.subscribe("#");
  initlora();
}

void loop()
{   
  // do anything here
  delay(2000);
}
