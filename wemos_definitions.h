// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 868.0
//Lora.h
// Dragino_LoRa_v1.2 ports
#define SCK     14    // GPIO5  -- SX1278's SCK
#define MISO    12   // GPIO19 -- SX1278's MISO
#define MOSI    11   // GPIO27 -- SX1278's MOSI
#define SS      15   // GPIO18 -- SX1278's CS
#define RST     0   // GPIO14 -- SX1278's RESET
#define DI0     16   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    915E6
